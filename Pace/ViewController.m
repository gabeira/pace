//
//  ViewController.m
//  Chronus
//
//  Created by Gabriel Pereira on 12/4/12.
//  Copyright (c) 2012 Gabriel Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)calcula:(id)sender {
    //Esconde teclado
    [self.distance resignFirstResponder];
    
    //NSInteger tt = self.tempo.minuteInterval;
    //NSLog(@"%d", tt);
    NSLog(self.tempo.description,nil);
    self.resultado.text = self.distance.text;
}
- (void)viewDidUnload {
    [self setTempo:nil];
    [self setTempo:nil];
    [super viewDidUnload];
}
@end
