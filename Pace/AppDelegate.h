//
//  AppDelegate.h
//  Pace
//
//  Created by Gabriel Pereira on 12/4/12.
//  Copyright (c) 2012 Gabriel Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
