//
//  ViewController.h
//  Chronus
//
//  Created by Gabriel Pereira on 12/4/12.
//  Copyright (c) 2012 Gabriel Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *distance;

@property (weak, nonatomic) IBOutlet UILabel *resultado;

@property (weak, nonatomic) IBOutlet UIPickerView *tempo;


- (IBAction)calcula:(id)sender;

@end
